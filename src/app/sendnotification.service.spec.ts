import { TestBed } from '@angular/core/testing';

import { SendnotificationService } from './sendnotification.service';

describe('SendnotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendnotificationService = TestBed.get(SendnotificationService);
    expect(service).toBeTruthy();
  });
});
